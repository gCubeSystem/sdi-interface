package org.gcube.spatia.data.model.profiles;

import java.util.Collections;
import java.util.Map;
import java.util.Map.Entry;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@RequiredArgsConstructor
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class ApplicationProfile {

	public static interface Properties{
		public static final String GEOSERVER_STORAGE="geoserver.storage";
		public static final String GEOSERVER_WORKSPACE="geoserver.workspace";
		
		public static final String GEONETWORK_CATEGORY="geonetwork.category";
		public static final String GEONETWORK_STYLE="geonetwork.style";
		
		public static final String THREDDS_CATALOG="thredds.catalog";
	}
	
	
	@NonNull
	private String serviceClass;
	@NonNull
	private String serviceName;
	@NonNull
	private Map<String,String> properties;
	
	
	public String getProperty(String propertyName) {
		return properties==null?null:properties.get(propertyName);
	}


	@Override
	public String toString() {
		return "ApplicationProfile [serviceClass=" + serviceClass + ", serviceName=" + serviceName + ", properties="
				+ properties + "]";
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		
		
		if(properties!=null && !properties.isEmpty())
			for(Entry<String,String> prop:properties.entrySet()) {
				result = prime * result + ((prop.getKey() == null) ? 0 : prop.getKey().hashCode());
				result = prime * result + ((prop.getValue() == null) ? 0 : prop.getValue().hashCode());
			}
		
		result = prime * result + ((properties == null) ? 0 : properties.hashCode());
		result = prime * result + ((serviceClass == null) ? 0 : serviceClass.hashCode());
		result = prime * result + ((serviceName == null) ? 0 : serviceName.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ApplicationProfile other = (ApplicationProfile) obj;
		
		
		
		if (properties == null) {
			if (other.properties != null)
				return false;
		} else {
			if(properties.size()!= other.properties.size()) return false;
			for(Entry<String,String> prop:properties.entrySet()) {
				if(!other.properties.containsKey(prop.getKey())) return false;
				if(!prop.getValue().equals(other.properties.get(prop.getKey()))) return false;
			}
		}
		
		
		if (serviceClass == null) {
			if (other.serviceClass != null)
				return false;
		} else if (!serviceClass.equals(other.serviceClass))
			return false;
		if (serviceName == null) {
			if (other.serviceName != null)
				return false;
		} else if (!serviceName.equals(other.serviceName))
			return false;
		return true;
	}
	
	
	
}
