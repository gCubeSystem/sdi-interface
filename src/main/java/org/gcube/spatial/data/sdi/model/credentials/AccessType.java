package org.gcube.spatial.data.sdi.model.credentials;

public enum AccessType {
	
	ADMIN,
	CONTEXT_MANAGER,
	CONTEXT_USER,
	CKAN;

}
