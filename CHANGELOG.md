This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

# Changelog for sdi-interface

## [v1.2.0] - 2020-09-03

### Fixes

- Integration with gcube distribution (https://support.d4science.org/issues/19612)